-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.41-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных phlacon
CREATE DATABASE IF NOT EXISTS `phlacon` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `phlacon`;


-- Дамп структуры для таблица phlacon.robots
CREATE TABLE IF NOT EXISTS `robots` (
  `ID` int(12) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) DEFAULT NULL,
  `TYPE` varchar(200) DEFAULT NULL,
  `YEAR` int(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы phlacon.robots: ~5 rows (приблизительно)
DELETE FROM `robots`;
/*!40000 ALTER TABLE `robots` DISABLE KEYS */;
INSERT INTO `robots` (`ID`, `NAME`, `TYPE`, `YEAR`) VALUES
	(1, 'ASIMO', 'HUMAN', 2002),
	(6, 'tezt2', 'spider2', 2015),
	(7, 'tezt2', 'spider2', 2015),
	(8, 'tezt', 'spider', 2015),
	(9, 'tezt', 'spider', 2015);
/*!40000 ALTER TABLE `robots` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
