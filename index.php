<?php
//test CI
use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

try {
    $loader = new Loader();
    $loader->registerDirs(array(
        'app/controllers/',
        'app/models/',
        'app/services/'
    ))->register();

    $di = new FactoryDefault();

    $di->set('db', function () {
        return new DbAdapter(array(
            "host"     => "localhost",
            "username" => "root",
            "password" => "",
            "dbname"   => "phlacon"
        ));
    });

    //Меняем роутер так чтобы он принимал параметр на индексный метод
    $di->set('router', function(){
        $router = new \Phalcon\Mvc\Router();
        $router->add(
            '/{controller}/{id:\d+}',
            array(
                "action" => "index",
            )
        );

        return $router;
    });

    $di->set('request', new \Phalcon\Http\Request());

    $di->set('view', function () {
        $view = new View();
        $view->setViewsDir('app/views/');
        return $view;
    });

    $application = new Application($di);

    echo $application->handle()->getContent();

} catch (\Exception $e) {
    echo "PhalconException: ", $e->getMessage();
}