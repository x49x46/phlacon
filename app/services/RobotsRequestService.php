<?php

class RobotsRequestService extends \RobotsService
{
    public function handle($id)
    {
        if ($this->request->isGet()) {
            return $this->onGet($id);
        }

        if ($this->request->isPost()) {
            return $this->onPost();
        }

        if ($this->request->isPut()) {
            return $this->onPut($id);
        }

        if ($this->request->isDelete()) {
            return $this->onDelete($id);
        }
    }

    private function onGet($id = 0)
    {
        if ($id) {
            $robots = Robots::findFirst($id);
        } else {
            $robots = Robots::find();
        }
        return $this->complete($robots);
    }

    private function onPost()
    {
        $robot = new Robots();

        $robot->NAME = $this->request->getPost('name');
        $robot->TYPE = $this->request->getPost('type');
        $robot->YEAR = $this->request->getPost('year');
        $robot->save();
    }

    private function onPut($id)
    {
        if ($id) {
            $robot = Robots::findFirst($id);

            if (count($robot) > 0) {
                $robot->NAME = $this->request->getPut('name');
                $robot->TYPE = $this->request->getPut('type');
                $robot->YEAR = $this->request->getPut('year');
                $robot->save();
            }
        }
    }

    private function onDelete($id)
    {
        if ($robot = Robots::findFirst($id)) {
            $robot->delete();
        }
    }
}