<?php

use \BaseService;

class RobotsService extends BaseService
{

    /**
     * ���������� � ����� ������
     *
     * @param $robots
     */
    public function complete($robots)
    {
        if ($robots and count($robots) > 0) {
            $robots = $this->prepareDataOutput($robots);
            return $this->output($robots);
        } else {
            return $this->errorMessage();
        }
    }

    /**
     * ���������� ������ � ������
     *
     * @param $robots
     * @return array
     */
    private function prepareDataOutput($robots)
    {
        $allData = array();

        if (is_a($robots, 'Robots')) {
            $robots = array($robots);
        }

        foreach ($robots as $robot) {
            $data = new StdClass();

            $data->id = $robot->ID;
            $data->name = $robot->NAME;
            $data->type = $robot->TYPE;
            $data->year = $robot->YEAR;

            $allData[] = $data;

            unset($data);
        }

        return $allData;
    }
}