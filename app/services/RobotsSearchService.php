<?php

class RobotsSearchService extends \RobotsService
{
    public function handle($name)
    {
        if ($name) {
            $robots = Robots::find(array('conditions' => "NAME LIKE '%$name%'"));
            return $this->complete($robots);
        } else {
            return $this->errorMessage();
        }
    }
}