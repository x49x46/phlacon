<?php

class BaseService extends \Phalcon\DI\Injectable
{
    protected function errorMessage($message = 'Not found')
    {
        $error = array(
            'status' => 'error',
            'message' => $message
        );

        return $this->output($error);
    }

    protected function output($data)
    {
        return json_encode($data);
    }
}