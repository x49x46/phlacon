<?php

use \RobotsRequestService as RequestService;
use \RobotsSearchService as SearchService;

class RobotsController extends BaseController
{
    public function indexAction($id = 0)
    {
        $this->view->output = (new RequestService($this))->handle($id);
    }

    public function searchAction($name = '')
    {
        $this->view->output = (new SearchService())->handle($name);
    }
}