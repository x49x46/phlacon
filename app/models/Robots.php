<?php

use Phalcon\Mvc\Model;

class Robots extends Model
{
    public $id;
    public $name;
    public $type;
    public $year;
}